import urllib.request
import digdag

API_URL = "http://s-proj.com/utils/checkHoliday.php"


class CheckMarketHoliday(object):
    def store_result(self, label, time):
        params = {
            "opt": "market",
        }
        req = urllib.request.Request(
            "{}?{}".format(API_URL, urllib.parse.urlencode(params))
        )
        print(req.get_full_url())
        with urllib.request.urlopen(req) as res:
            print(res.getcode())
            print(res.info())
            body = res.read()
        print(body.decode("utf-8"))
        # 休日であればholiday、そうでなければelseを格納
        digdag.env.store({"is_holiday": body.decode("utf-8")})
